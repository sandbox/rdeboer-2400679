<?php

/**
 * @file
 * Contains the basic shipping pseudo-field handler for Commerce orders
 *
 * @todo: Add formatter drop-down offering choice of Raw and Formatted
 */

/**
 * Field handler to provide simple renderer of shipping cost.
 */
class commerce_bagatelles_handler_field_shipping_order extends views_handler_field_numeric {

  /**
   * Call constructor.
   */
  public function construct() {
    parent::construct();
    $this->additional_fields['order_id'] = 'order_id';
  }

  /**
   * Help build the query.
   */
  public function query() {
    // Overridding query() to avoid parent::query() being called as that would
    // treat 'shipping' as a real database field, causing errors.
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  /**
   * Render the shipping value.
   *
   * @param object $values, as returned by the Views query
   *
   * @return string
   */
  function render($values) {
    $order_id_alias = $this->aliases['order_id'];
    $order_id = $values->$order_id_alias;
    // Alternatively find any field that ends with 'order_id'.

    $order = commerce_order_load($order_id);
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    $shipping_total = 0.00;
    foreach ($order_wrapper->commerce_line_items as $line_item_wrapper) {
      $type = $line_item_wrapper->type->value();
      if ($type == 'shipping') {
        $shipping_total += $line_item_wrapper->commerce_total->amount->value();
      }
    }
    return number_format($shipping_total * 0.01, 2);
  }
}
