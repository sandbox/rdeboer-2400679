<?php

/**
 * @file
 * Contains the basic shipping pseudo-field handler for Commerce line items.
 */

/**
 * Field handler to provide simple renderer of shipping cost.
 */
class commerce_bagatelles_handler_field_shipping_line_item extends views_handler_field_numeric {

  /**
   * Help build the query.
   */
  public function query() {
    // Overridding query() to avoid parent::query() being called as that would
    // treat 'shipping' as a real database field, causing errors.
  }

  /**
   * Render the shipping value.
   *
   * @param object $values, as returned by the Views query
   *
   * @return string
   */
  function render($values) {
    // Find the shipping line item in the order and return its rendered value.
    $shipping_amount = NULL;
    if (isset($values->_field_data)) {
      $line_item = $values->_field_data['line_item_id'];
      $line_item_wrapper = entity_metadata_wrapper($line_item['entity_type'], $line_item['entity']);
      $type = $line_item_wrapper->type->value();
      if ($type == 'shipping') {
        $shipping_amount = $line_item_wrapper->commerce_total->amount->value();
        //$formatted = commerce_currency_format($shipping_amount->amount->value(), $shipping_amount->currency_code->value(), $line_item_wrapper);
      }
    }
    return isset($shipping_amount) ? number_format($shipping_amount * 0.01, 2) : NULL;
  }
}
