<?php

/**
 *  Make some special Commerce order fields available in Views.
 */

/**
 * Implements hook_views_data_alter()
 */
if (module_exists('commerce_shipping')) {

  function commerce_bagatelles_views_data_alter(&$data) {
    // Expose shipping cost of a line item
    $data['commerce_line_item']['shipping'] = array(
      'title' => t('Shipping cost'),
      'help' => t('The shipping cost, if this is a shipping line item, void otherwise.'),
      'field' => array(
        'handler' => 'commerce_bagatelles_handler_field_shipping_line_item',
        'click sortable' => TRUE,
      ),
    );
    // Expose shipping cost on commerce_order (or should this be commerce_payment_transaction?)
    $data['commerce_order']['shipping'] = array(
      'title' => t('Shipping cost', array(), array('context' => 'a drupal commerce order')),
      'help' => t('The sum total of all shipping components on the order.'),
      'field' => array(
        'handler' => 'commerce_bagatelles_handler_field_shipping_order',
        'click sortable' => TRUE,
      ),
    );
  }
}