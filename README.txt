
COMMERCE BAGATELLES
===================

bagatelle |ˌbagəˈtɛl| noun:
"something regarded as too unimportant to be worth much consideration"

This module contains a mixed bag of bagatelles for the Commerce suite.
Little trifles and trinkets to address common niggly bits.
Here's a summary.

o Change the names of existing order statuses, for example you can change
  'Pending' to 'Paid, not shipped' (requires you to edit the .module file).
  Note: to add NEW statuses (for existing states), use the module
  commerce_custom_order_status.
  To introduce an additional state+status to mark when payment is received in
  full enable module commerce_paymentreceived.

o Rules actions to lock a line item or an entire order in the cart. Once added
  to the cart visitors cannot change the item quantity or remove it. This comes
  in handy for items that are added programmatically based on special conditions
  that must not be violated by changes made through the UI. One example is
  products that must be bought in pairs (you must buy product A to receive
  product B). Another are products that must be bought in a fixed quantity, like
  memberships (1) or bottles of wine (a dozen or half dozen).

o For order reports created through Views, a field to display the Shipping costs
  of an order as a column, rather than a shipping line item row (requires
  commerce_shipping).

o Making sure that the tax row in the order summary is always last, just above
  the order total. When items like discounts and shipping present, the tax row
  can end up in the middle, which is not always the best look.

o Variables handy for use in theming/templating. For example,
  $variables['cart_items'] and $variables['cart_value'] are available in themes
  automatically to display the number of items currently in the cart and their
  total monetary value (formatted a price in the relevant currency).

o Programmatically, as opposed to through the form UI, put products into the
  visitor's shopping cart with only one call:
    commerce_bagatelles_add_to_cart($sku, $quantity)
