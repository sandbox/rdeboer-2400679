<?php
/**
 * @file
 * Rules Actions for Commerce Bagatelles.
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_bagatelles_rules_action_info() {
  $actions = array();

  $actions['commerce_bagatelles_lock_line_items'] = array(
    'label' => t('Lock line item in the cart'),
    'parameter' => array(
      'commerce_line_item' => array(
        'type' => 'commerce_line_item',
        'label' => t('Line item'),
      ),
    ),
    'group' => t('Commerce Bagatelles'),
    'callbacks' => array(
      'execute' => 'commerce_bagatelles_lock_line_item',
    ),
  );

  $actions['commerce_bagatelles_make_cart_readonly'] = array(
    'label' => t('Make cart read-only'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
    ),
    'group' => t('Commerce Bagatelles'),
    'callbacks' => array(
      'execute' => 'commerce_bagatelles_make_cart_readonly',
    ),
  );

  return $actions;
}

/**
 * Lock a line item on the shopping cart.
 * 
 * @param object $line_item, Commerce order line item object.
 */
function commerce_bagatelles_lock_line_item($line_item) {
  if (empty($_SESSION['commerce_bagatelles']['locked_line_item_ids']) ||
    !in_array($line_item->line_item_id, $_SESSION['commerce_bagatelles']['locked_line_item_ids'])) {
    $_SESSION['commerce_bagatelles']['locked_line_item_ids'][] = $line_item->line_item_id;
  }
}

/**
 * Mark an order's cart as read-only.
 *
 * This will lock all line items, disabling the Remove buttion on each as well
 * as the Update Cart button at the bottom.
 *
 * @param object $order, Commerce order object
 */
function commerce_bagatelles_make_cart_readonly($order) {
  if (empty($_SESSION['commerce_bagatelles']['cart_readonly_order_ids']) ||
    !in_array($order->order_id, $_SESSION['commerce_bagatelles']['cart_readonly_order_ids'])) {
    $_SESSION['commerce_bagatelles']['cart_readonly_order_ids'][] = $order->order_id;
  }
}